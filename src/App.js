import React, {useState, useEffect} from 'react';
import axios  from 'axios';


function Phrase({phrase}) {
    return (
        <div className={"frase"}>
            <h1>{phrase.quote}</h1>
            <p>- {phrase.author}</p>
        </div>
    )
}

function App() {

    const [phrase, getPhrase] =  useState({}); //phrase = state={} and getPhrase = this.setState

    const queryApi = async () => {
        const result = await axios('https://breaking-bad-quotes.herokuapp.com/v1/quotes');
        //Add API result to state similar to this.setState
        getPhrase(result.data[0]);
    };

    //Rest API request
    //Replace to componentDidMount() and componentDidUpdate()
    useEffect(
        () => {
            queryApi()
        }, []
    );

    return (
        <div className={"contenedor"}>
            <Phrase
                phrase={phrase}
            />
            <button onClick={queryApi}>Generar Nueva</button>
        </div>
    )
}

export default App;